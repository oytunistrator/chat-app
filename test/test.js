'use strict'

var expect = require('chai').expect
  , server = require('../index')
  , io = require('socket.io-client')
  , ioOptions = { 
      transports: ['websocket']
    , forceNew: true
    , reconnection: false
  }
  , user = 'Oytun'
  , message = 'Selam!'
  , sender
  , receiver



describe('Connection', function(){
  beforeEach(function(done){
    server.start()
    sender = io('http://localhost:3000/', ioOptions)
    receiver = io('http://localhost:3000/', ioOptions)
    done()
  })
  afterEach(function(done){
    sender.disconnect()
    receiver.disconnect()
    done()
  })

  describe('Login', function(){
    it('Check User', function(done){
      sender.emit('login', user)
      receiver.on('login', function(u){
        expect(u).to.equal(user)
        done()
      })
    })
  })
})