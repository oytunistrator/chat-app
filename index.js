var express = require('express')
, app = express()
, port = process.env.PORT || 3000
, http = require('http').Server(app)
, io = require('socket.io')(http)

io.on('connection', function(socket){
    var online = 0;
    var logined = false;

    socket.on('disconnect', function(){
      if(logined){
        --online;
        console.log(socket.username+' disconnected');
      }
    });

    socket.emit('login', function(user){

      if(logined) return;

      socket.username = user;
      ++online;

      logined = true;

      io.sockets.emit('status', {
        username: socket.username,
        online: online
      })
    });

    socket.emit('message', function(message){
      io.sockets.emit('message', {
        username: socket.username,
        message: message
      })
    });
  });


  exports.server = http.listen(port)